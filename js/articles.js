//ARTICLES 
$(document).ready(function (){

	function jsonObjectCheck(jsonObject, output1 = null, output2 = "") {
		var output; 
		if (jsonObject != undefined) {
			if (output1 != null) {
				try{
					output = eval(output1);
				}
				catch(err){
					console.log(err);
					output = output1;
				}
			}
			else {
				output = jsonObject;
			}
		}
		else{
			output = output2;
		}
		
		return output;
	}

	function getMilS(dateString) {
		var dateParts = dateString.split(".");
		var partsNumber = dateParts.length;
		var year = 1970;
		var month = 0; 
		var day = 1;

		if (partsNumber > 1) {
			year = parseInt(dateParts[partsNumber-2]);
			if (partsNumber > 2) {
				month = parseInt(dateParts[partsNumber-3]) - 1;
				if (partsNumber > 3) {
					day = parseInt(dateParts[partsNumber-4]);
				}
			}
		}

		var date = new Date(year, month, day);

		return date.getTime();
	}


	//FILTERS 
	$("#filters").hide();

	$("#btnFilters").click(function(){
		$("#filters").slideToggle();
	});

	function brandFilterInput(phones) {
		let brands = []; 

		phones.forEach(phone => {
			if(brands.indexOf(phone.category) == -1) {brands.push(phone.category)}
		});

		if(brands.length){
			let lista = `
				<fieldset>
				<legend>Brands</legend>
				<ul>`;
			brands.forEach(brand => {
				lista += '<li><input type="checkbox" value="'+brand+'" name="brands"/>'+brand+'</li>';
			});
			lista += "</ul></fieldset>";
			$(lista).insertBefore("#displayType");
		}
	}

	function batteryFilterInput(phones) {
		let batteryCapacity = []; 

		phones.forEach(phone => {
			if(batteryCapacity.indexOf(phone.specs.BATTERY.capacity) == -1) {batteryCapacity.push(phone.specs.BATTERY.capacity)}
		});
		batteryCapacity.sort((x, y)=>x-y);
		let bCArrayLength = batteryCapacity.length;

		if (bCArrayLength) {
			let range = `
				<fieldset>
				<legend>Battery Capacity</legend>`;
			range += `<datalist id="bCList">`;
			batteryCapacity.forEach(bC => range += `<option value="${bC}" label="${bC}"></option>`);
			range += `</datalist>`;

			let bCArrayMin = batteryCapacity[0]; //min="${bCArrayMin}"
			let bCArrayMax = batteryCapacity[bCArrayLength-1]; //max="${bCArrayMax}"
			range += `
				<label for="rngMinVal">MIN</labe><br/>
				<label for="rngMinVal">${bCArrayMin}</label>
				<input type="range" min="${bCArrayMin}" max="${bCArrayMax}" list ="bCList" value="${bCArrayMin}" id="rngMinVal"/>
				<label for="rngMinVal">${bCArrayMax}</label>
				<br/><br/>
				<label for="rngMaxVal">MAX</labe><br/>
				<label for="rngMaxVal">${bCArrayMin}</label>
				<input type="range" min="${bCArrayMin}" max="${bCArrayMax}" list ="bCList" value="${bCArrayMax}" id="rngMaxVal"/>
				<label for="rngMaxVal">${bCArrayMax}</label>`;
			range += `</fieldset>`;

			$(range).insertBefore("#displayType");
		}
	} 

	function createFilters(phones) {
		
		brandFilterInput(phones);
		
		batteryFilterInput(phones);
	}
	ajaxCall("phones", createFilters); 
	
	function brandFilter(phones) {
		var brands = [];
		$('input[name="brands"]:checked').each(function (chb) {
			brands.push($(this).val()); 
		}); 

		if (brands.length != 0){
			phones = phones.filter(phone => brands.includes(phone.category));
		}
		return phones;	
	}

	function batteryFilter(phones){
		let minVal = $("#rngMinVal").val();
		let maxVal = $("#rngMaxVal").val();	

		if (maxVal >= minVal) {
			phones = phones.filter(phone => minVal <= phone.specs.BATTERY.capacity && phone.specs.BATTERY.capacity <= maxVal );
		}
		else{
			alert("MIN veci od MAXa!");
		}

		return phones;
	}

	function displayFilter(phones) {
		let displayType = $('input[name="displayTypes"]:checked').val();
		
		if (displayType != undefined) {
			phones = phones.filter(phone => phone.specs.DISPLAY.type.toLowerCase().indexOf(displayType.toLowerCase()) != -1);
		}

		return phones;
	}


	//SORT
	function sortParametersSetUp(){ 
		var parameters = null;
		switch(sortOptions.value){
			case "nameAsc": parameters = {criteria:"name", a:1, b:-1}; break; 
			case "nameDesc": parameters = {criteria:"name", a:-1, b:1}; break; 
			case "dateDesc": parameters = {criteria:"date", a:-1, b:1}; break; 
			case "dateAsc": parameters = {criteria:"date", a:1, b:-1}; break; 
		}
		return parameters;
	}

	function sortCategories() {
		if (sortOptions.value == "nameAsc" || sortOptions.value == "nameDesc") {
			var parameters = sortParametersSetUp();

			if (parameters != null) {
				var categories = $(".category");

				categories.sort(function (category1, category2) {
					var category1_name = $(category1).find(".category-name").text();
					var category2_name = $(category2).find(".category-name").text();

					if (category1_name > category2_name) {return parameters.a;}
					else if (category1_name < category2_name) {return parameters.b;}
					else {return 0;}
				});

				$("#categories").html(categories);
			}
		}
		
	}

	function sortArticles(articles){
		if (sortOptions.value != "none") {
			var parameters = sortParametersSetUp();

			if (parameters != null) { 
				if (parameters.criteria == "name") {
					articles.sort(function (article1, article2) {
						if (article1.model > article2.model) {return parameters.a;}
						else if (article1.model < article2.model) {return parameters.b;}
						else {return 0;}
					});
				}
				else if(parameters.criteria == "date"){
					articles.sort(function (article1, article2) {
						let date1 = getMilS(article1.specs.AVAILABILITY.released);
						let date2 = getMilS(article2.specs.AVAILABILITY.released);

						if (date1 > date2) {return parameters.a;}
						else if (date1 < date2) {return parameters.b;}
						else {return 0;}
					});
				}
			}
		}
		
		return articles;
	}


	//DISPLAYING CONTENT
	function createCategorie(categoryName) {
		var categories = document.getElementById('categories');

		var categoryDiv = document.createElement('div'); 
		categoryDiv.classList.add("page");
		categoryDiv.classList.add("cont"); 
		categoryDiv.classList.add("category"); 
		categoryDiv.id = categoryName;
		categoryDiv.innerHTML = '<h2 class="category-name">'+categoryName+'</h2>';

		categories.appendChild(categoryDiv);
	}

	function createArticles(phones) { 
		phones = brandFilter(phones);
		phones = batteryFilter(phones);
		phones = displayFilter(phones);
		phones = sortArticles(phones); 

		subMenuCreate(phones, {menuItemId:"phonesMenu"});

		$("#categories").html("");

		var article, category, memoryVariants, selfieSensor, boxNum;

		for (var i = 0; i < phones.length; i++) {

			article = document.createElement('div'); 
			article.id = phones[i].model.replace(/\s+/g, "");
			category = phones[i].category.trim();
			memoryVariants = phones[i].specs.MEMORY.variants;
			selfieSensor = phones[i].specs.CAMERAS.SELFIE.sensors[0].wide; 

			article.innerHTML += '<h3>'+phones[i].model+'</h3>'
								+'<div class="box">'
									+'<a href="#" class="image image-full">'
										+'<img src="'+phones[i].imgAdress+'" alt="'+category+' '+phones[i].model+'" />'
									+'</a>'
									+'<span>'
										+phones[i].specs.DISPLAY.type+" ("+phones[i].specs.DISPLAY.size+"\")"
										+"<br/>"
										+phones[i].specs.CHIPSET[0].name+" ("+phones[i].specs.CHIPSET[0].arhitecture+")"
										+jsonObjectCheck(phones[i].specs.CHIPSET[1], '"<br/>/<br/>"+jsonObject.name+" ("+jsonObject.arhitecture+")"')+"<br/>"
										+memoryVariants[memoryVariants.length-1].RAM+"/"+memoryVariants[memoryVariants.length-1]["Internal storage"]+" GB<br/>"
										+"MAIN CAMERA: "+phones[i].specs.CAMERAS.MAIN.sensors[0].wide.resolution+" MP <br/>"
										+"SELFIE CAMERA: "+jsonObjectCheck(selfieSensor.resolution, (selfieSensor.resolution+" MP"), selfieSensor.desc)+"<br/>"
										+phones[i].specs.BATTERY.capacity+" mAh"
									+'</span>'
								+'</div>'
								;

			if (document.getElementById(category) == null) {
				createCategorie(category);
			}
			boxNum = document.querySelectorAll("#"+category+" > div").length % 3 + 1;
			article.classList.add("box"+boxNum); 
			document.getElementById(category).appendChild(article); 
		}

		sortCategories(); 
		scrollToElement();
	}

	ajaxCall("phones", createArticles); 

	$("#btnDoFilters").click(function() { 
		ajaxCall("phones", createArticles); 
	});

	$("#sortOptions").change(function() { 
		ajaxCall("phones", createArticles); 
	});


	
}); 