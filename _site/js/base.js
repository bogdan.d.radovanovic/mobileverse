$(document).ready(function(){

	//Menu active link
	window.urlParts = document.URL.split("/");
	window.currentPage = urlParts[urlParts.length - 1];

	var menuItems = document.querySelectorAll("#menu > ul > li");
	for (let i = 0; i < menuItems.length; i++) {
		let linkParts = menuItems[i].querySelector("a").href.split("/");

		if(linkParts[linkParts.length -1] == currentPage){
			menuItems[i].classList.add("active");
		}
	}

	window.scrollToElement = function (elementId = "") {

		if (elementId == "") {
			if (localStorage) {
				elementId = localStorage.getItem("scrollElementId");
				if (elementId != null) {
					localStorage.removeItem("scrollElementId");
					document.getElementById(elementId).scrollIntoView();
				}
			}
		}
		else if (currentPage == "index.html") {
			document.getElementById(elementId).scrollIntoView();
		}
		else{console.log(urlParts);
			if (localStorage) {
				localStorage.setItem('scrollElementId', elementId);
				
				urlParts.pop();
				console.log(urlParts);
				window.location.href = urlParts.join("/")+"/index.html";
			}
		}
	}

	window.ajaxCall = function (fileName, callback, args = {}) {
		$.ajax({
			url: "data/"+fileName+".json", 
			method: "GET", 
			dataType: "JSON", 
			success: function(data){
				callback(data, args);
			}, 
			error: function(err){
				console.log(err);

			}
		});
	}


	window.subMenuCreate = function (phones, args = {menuItemId:""}) {
		
		var menu = document.getElementById(args.menuItemId);
		var subMenu = menu.querySelector(".subMenu");
		if (subMenu != null) {
			menu.removeChild(subMenu);
		}

		var newSubMenu = document.createElement("ul");
		newSubMenu.classList.add("subMenu");
		menu.appendChild(newSubMenu);

		for (var i = 0; i < phones.length; i++) {
			let link = document.createElement("a");
			link.setAttribute("href", "#");
			link.style.color = "#8dc63f";
			link.textContent = phones[i].model.trim();

			let listElement = document.createElement("li");
			let phoneDivId = phones[i].model.replace(/\s+/g, "");
			listElement.id = "subMenu"+phoneDivId;
			listElement.appendChild(link);

			let categoryName = phones[i].category.trim();
			let categoryLi = document.getElementById("menu"+categoryName);
			//First level of menu
			if (categoryLi == null) {
				let categoryLink = document.createElement("a");
				categoryLink.setAttribute("href", "#");
				categoryLink.textContent = categoryName;

				let categoryListElement = document.createElement("li");
				let categoryDivId = categoryName.replace(/\s+/g, "");
				categoryListElement.id = "menu"+categoryDivId;
				categoryListElement.appendChild(categoryLink);
				categoryListElement.appendChild(document.createElement("ul"));

				menu.querySelector(".subMenu").appendChild(categoryListElement);
				menu.querySelector(".subMenu").appendChild(document.createElement("br"));

				document.querySelector("#"+categoryListElement.id+" a").addEventListener("click", function (e) {
					e.preventDefault();
					scrollToElement(categoryDivId);
				});
			}
			
			document.querySelector("#menu"+categoryName+" ul").appendChild(listElement);

			document.getElementById(listElement.id).querySelector("a").addEventListener("click", function (e) {
				e.preventDefault();
				scrollToElement(phoneDivId);
			});
		}

		//Menu animations
		$('#menu li ul').hide(); 
		
		$('#menu li').hover(function(){
			$(this)
				.find('.subMenu')
				.stop(true, true)
				.slideDown();
		}, function(){
			$(this)
				.find('.subMenu')
				.stop(true, true)
				.slideUp("fast");
		});

		$('.subMenu li').hover(function(){
			$(this)
				.find('ul')
				.stop(true, true)
				.slideDown();
		}, function(){
			$(this)
				.find('ul')
				.stop(true, true)
				.slideUp("fast");
		});
	}
});

