window.onload = function () {

	$("#chbNotifications").change(function(){
		$("#notificationsFrequency").toggle();
	});

	$("#btnDeleteCookie").click(function(){
		removeCookie();
	});

	//Drop-Down List
	var ageRanges = new Array ("-Select age-", 
							 "Under 18", 
							 "18-29", 
							 "30-49", 
							 "50-69",
							 "70+");
	var ddList = document.createElement("select");
	ddList.setAttribute("id", "ddlAge");
	ddList.setAttribute("name", "ddlAge");
	ddList.setAttribute("class", "inputElement");

	var ddlOption;
	for (var i = 0; i < ageRanges.length; i++) {
		ddlOption=document.createElement("option");
		ddlOption.setAttribute("value", i);
		ddlOption.textContent = ageRanges[i];
		ddList.appendChild(ddlOption);
	}
	var list = document.getElementById("list");
	list.appendChild(ddList);
	var ddlLabel = document.createElement("label");
	ddlLabel.setAttribute("for", "ddlAge");
	ddlLabel.appendChild(document.createTextNode(" * "));
	ddlLabel.classList.add("errorLabel");
	list.appendChild(ddlLabel);



	//Focus on fisrt empty input element
	function emptyInputFocus() {
		var inputElements = document.getElementsByClassName('inputElement');

		var val;
		for (var i = 0; i < inputElements.length; i++) {
			val = inputElements[i].value;
			if (val == "" 
				|| val == "0" 
				|| (inputElements[i].name == "chbNotifications" && inputElements[i].checked == false) 
				|| document.getElementsByName('rbNotifications').checked == false 
				|| val == "Send") 
			{
				inputElements[i].focus();
				break;
			}
		}
	}
	emptyInputFocus();



	//Comments
	getComments();

	
	//Auto-fill from cookie
	window.regEmail = /^[\w\d\._-]{3,}@\w{3,}(\.\w{2,3})+$/;
	function userInfoFill() {
		var email = $("#tbEmail").val().trim();
		if(regEmail.test(email) && document.cookie != "")
		{
			var cookies = document.cookie.split("; ");
			for (var i = 0; i < cookies.length; i++) {
				var cookieName = cookies[i].split(";")[0].split("=")[0];
				var cookieValue = JSON.parse(cookies[i].split(";")[0].split("=")[1]);

				if (cookieName == email) {
					$("#tbName").val(cookieValue.name);
					$("#tbNumber").val(cookieValue.phone);
					document.getElementById("ddlAge").options[cookieValue.age].selected = true;
					if (cookieValue.notifications != "No") {
						if(document.getElementById("chbNotifications").checked == false){
							$("#chbNotifications").click()
						} 
						document.getElementById(cookieValue.notifications).checked = true;
					}
				}
			}

			$("#btnDeleteCookie").show();

			emptyInputFocus();

			getComments(email);
		}
	}

	//$("#tbEmail").blur(userInfoFill);
	$("#tbEmail").keypress(function(event) {
		if (event.key === "Enter") {
			event.preventDefault();
			userInfoFill();
		}
	});
}



//Cookies
function setCookie(name, value) {
	var expireDate = new Date();
	expireDate.setFullYear(expireDate.getFullYear()+1); 

	var cookieDate = expireDate.toGMTString();
	var myCookie = name + "=" + value + ";expires=" + cookieDate;
	document.cookie = myCookie;
}

function removeCookie() {
	var email = $("#tbEmail").val().trim();

	if(regEmail.test(email) && document.cookie != "")
	{
		var expireDate = new Date();
		expireDate.setTime(expireDate.getTime()-1);

		var cookieDate = expireDate.toGMTString();
		var myCookie = email + "=;expires=" + cookieDate;
		document.cookie = myCookie;

		location.reload();
	}
}



//Local Storage
function setLocalStorage(email, comment) {
	if(localStorage){
		localStorage.setItem(email, JSON.stringify(comment));
	}
}

function setComment(email, comment) {
	if (localStorage) {
		var comments = localStorage.getItem(email)==null ? new Array() : JSON.parse(localStorage.getItem(email));
		comments.push(comment);
		localStorage.setItem(email, JSON.stringify(comments));
	}
}

function getLocalStorage(email) {
	if (localStorage) {
		if(localStorage.getItem(email) != null) {
			return JSON.parse(localStorage.getItem(email));
		}	
	}
}

function clearLocalStorage(email) {
	if (localStorage) {
		if (email != null) {
			if(localStorage.getItem(email) != null) {
				localStorage.removeItem(email);
			}
		}
		else{
			localStorage.clear();
		}
		document.getElementById("infoDiv").innerHTML = "";
	}
}

function removeComment(email, comment) {
	var userComments = getLocalStorage(email);

	if (userComments != null) {
		var commentIndex = userComments.indexOf(comment);
		if(commentIndex != -1){
			userComments.splice(commentIndex, 1);

			setLocalStorage(email, userComments);
		}
	}
}

function getComments(email=null) {
	if (localStorage) {
		var comments = new Array();
		var user;
		var commentsTable; 

		if(email == null){
			var keys = Object.keys(localStorage);

			if (keys != null) {
				for (let i = 0; i < keys.length; i++) {
					if (keys[i] != "scrollElementId") {
						user = JSON.parse(localStorage.getItem(keys[i]));
						for (let j = 0; j < user.length; j++) {
							comments.push({"user":keys[i], "comment":user[j]});
						}
					}
				}
			}
		}
		else{
			if (localStorage.getItem(email) != null) {
				user = JSON.parse(localStorage.getItem(email));
				for (let i = 0; i < user.length; i++) {
					comments.push({"user":email, "comment":user[i]});
				}
			}
		}

		if (comments != "") {
			commentsTable = `
							<table id="comments" border="3px">
							<thead>
								<tr>
									<th>User</th>
									<th colspan="2">Comment</th>
								</tr>
							</thead>
							<tbody>`;

			for (let i = 0; i < comments.length; i++) {
				commentsTable += `
								<tr>
									<td>${comments[i].user}</td>
									<td>${comments[i].comment}</td>`;
				if (email != null) {
					commentsTable += `
									<td>
										<button type="button" id="removeComment${i}">
											Remove
										</button>
									</td>`;
				}
									
				commentsTable += `</tr>`;
			}
			commentsTable += `
							</tbody></table>
							<button type="button" id="clearLocalStorage">Delete All</button>`;

			document.getElementById("infoDiv").innerHTML = commentsTable;

			if (email != null) {
				for (let i = 0; i < comments.length; i++) {
					$("#removeComment"+i).on("click", function(){
						removeComment(comments[i].user, comments[i].comment);
					});
				}
			}
			$("#clearLocalStorage").on("click", function(){
				clearLocalStorage(email);
			});
		}
		else{
			document.getElementById("infoDiv").innerHTML = "";
		}
	}
}



//Info check
function checkUp(){

	var taNote = document.getElementById("taNote");
	if (taNote.value == "") {
		taNote.classList.add("inputError");
		alert("You didn't write a message. Feel free to say us or ask anything. :) ");
		return false;
	}
	else{
		taNote.classList.remove("inputError");

		//RegEx
		var regName = /^[A-Z][a-z]{2,}(\s[A-Z][a-z]{2,})+$/;
		var regNumber = /^06([0-6]|9)\d{6,7}$/;

		var error = new Array();
		var user = {};

		//Name check
		var tbName = document.getElementById("tbName");
		var name = tbName.value.trim();

		if(regName.test(name)){
			user.name = name;
			tbName.classList.remove("inputError");
			tbName.labels[0].style.display = "none";
		}
		else{
			error.push(tbName);
		}

		//Email check
		var tbEmail = document.getElementById("tbEmail");
		var email = tbEmail.value.trim();
		if(regEmail.test(email)){
			tbEmail.classList.remove("inputError");
			tbEmail.labels[0].style.display = "none";
		}
		else{
			error.push(tbEmail);
		}

		//Number check
		var tbNumber = document.getElementById("tbNumber");
		var number = tbNumber.value.trim();
		if(regNumber.test(number)){
			user.phone = number;
			tbNumber.classList.remove("inputError");
			tbNumber.labels[0].style.display = "none";
		}
		else{
			error.push(tbNumber);
		}

		//Age check
		var ddlAge = document.getElementById("ddlAge");
		if(ddlAge.value != "0"){
			user.age = ddlAge.selectedIndex;
			ddlAge.classList.remove("inputError");
			ddlAge.labels[0].style.display = "none";
		}
		else{
			error.push(ddlAge);
		}

		//Notifications check
		var chbNotifications = document.getElementById("chbNotifications");
		var notifications;
		if (chbNotifications.checked) {

			//Notifications frequency check 
			var rbNotifications = document.form1.rbNotifications;
			if(rbNotifications.value != ""){ 
				for (var i = 0; i < rbNotifications.length; i++) {
					rbNotifications[i].labels[0].style.display = "none";
					if (rbNotifications.value == rbNotifications[i].value) {notifications = rbNotifications[i].id;}
				}
			}
			else{
				for (var i = 0; i < rbNotifications.length; i++) {
					error.push(rbNotifications[i]); 
				}
			}

		}
		else {
			notifications = "No";
		}
		user.notifications = notifications;



		//Final check
		if (error == 0) {
			setCookie(email, JSON.stringify(user));
			setComment(email, taNote.value.trim());
			$("#btnDeleteCookie").show();
			getComments(email);
			return false;
		}
		else{
			for (var i = 0; i < error.length; i++) {
				if(error[i].name != "rbNotifications"){
					error[i].classList.add("inputError");
				}
				error[i].labels[0].style.display = "inline";
			}
			return false;
		}
	}
}

document.getElementById("form1").onsubmit = checkUp;